import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Counter from "./components/Counter";
import PokemonCard from "./components/PokemonCard";
import TestingFun from "./components/TestingFun";
import HomeScreen from "./components/HomeScreen";

// stateless component -> functional component
// stateful component-> class component
import React, { Component } from "react";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogin: false,
    };
  }
  render() {
    return (
      <>
        {/* <Counter name = "Counter App Something Something" />
          <div className="container">
            <PokemonCard   />
          </div>
          <TestingFun something=" this is the pokemon card "/> */}
        <HomeScreen isLogin={this.state.isLogin} />
        <div className="text-center">
          <button
            className="btn btn-warning"
            onClick={() => this.setState({ isLogin: !this.state.isLogin })}
          >
            {this.state.isLogin ? "Logout" : "Login"}{" "}
          </button>
        </div>
      </>
    );
  }
}

export default App;
