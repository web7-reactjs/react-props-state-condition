import React from 'react';

// const TestingFun = ({something})
const TestingFun = (props) => {
    return (
        <div>
            <h1> This is the Testing Fun </h1>
             <h2>{ props.something}</h2>
        </div>
    );
}

export default TestingFun;
