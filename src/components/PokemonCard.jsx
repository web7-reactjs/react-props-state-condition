import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import React, { Component } from "react";
class PokemonCard extends Component {
  constructor(props) {

    super(props);
    this.state = {
      pokemon: {
        image:
          "https://i.pinimg.com/736x/35/a7/42/35a742e4a424a91397af4a0ae0872878.jpg",
        name: "Lotad Pokemon",
        description:
          "Some quick example text to build on the card title and make up the  bulk of the card content",
        btnText: "Promote",
      },
    };
  }
  
  render() {
    return (
      <Card style={{ width: "18rem" }}>
        <Card.Img variant="top" src={this.state.pokemon.image} />
        <Card.Body>
          <Card.Title> {this.state.pokemon.name}</Card.Title>
          <Card.Text>{this.state.pokemon.description}</Card.Text>
          <div className="text-center">
            <Button
              variant="primary"
              onClick={() =>
                this.setState({
                  pokemon: {
                    name: "Pikachu",
                    description:
                      " this is the animale that has yellow feather. and has the electricity shock...",
                    btnText: "Promoted",
                    image:"https://i.pinimg.com/originals/35/9c/89/359c89c0d3254dfdf8957089ebca6295.png"
                  },
                })
              }
            >
              {this.state.pokemon.btnText}
            </Button>
          </div>
        </Card.Body>
      </Card>
    );
  }
}

export default PokemonCard;
