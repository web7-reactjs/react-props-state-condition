import React, { Component } from "react";

class HomeScreen extends Component {
  render() {
    let isLoggedIn = this.props.isLogin
   
    return (
      <div>
        {isLoggedIn ? (
          <h1 className="text-center bg-warning">
             Successfully Login 
          </h1>
        ) : (
          <h1 className="text-center  bg-warning">
            {" "}
            Please Login !!!
          </h1>
        )}
      </div>
    );
  }
}

export default HomeScreen;
