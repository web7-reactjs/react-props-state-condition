import React, { Component } from "react";

// stateful component
class Counter extends Component {
  constructor(props) {
    console.log(" I am from the constructor of counter ")
    super(props);
    this.state = {
      counter: 0,
    };
  }
  componentDidMount(){
    console.log(" ------------- This is from component Did mount ------------------------")
  }

  componentDidUpdate(){
    console.log(" ---------------------- Component Will Update ---------------------------")
  }
  render() {
    return (
      <>
        <div className="text-center">
           <h2>{this.props.name}</h2> 
          <h1 className="bg-warning"> {this.state.counter}</h1>
          <button
            className="btn btn-warning "
            onClick={() => this.setState({ counter: this.state.counter + 1 })}
            style={{ marginRight: "10px" }}
          >
            Increase
          </button>
          <button
            className="btn btn-success"
            onClick={() => this.setState({ counter:  (this.state.counter==0 )? 0 :  this.state.counter - 1 })}
          >
            Decrease 
          </button>
        </div>
      </>
    );
  }
}

export default Counter;
